const expressAppUtil = require("../util/expressAppUtil");
const utilHttp = require("../util/utilHttp");
const constantes = require("../constantes.json");
const url = require('url'); 
const pathToFileURL = url.pathToFileURL;
const pathAPI = "/";
const fs = require("fs");
var proxy = require('express-http-proxy');
expressAppUtil.app.use('/', proxy('www.google.com'));