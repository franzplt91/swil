const expressAppUtil = require("../util/expressAppUtil");
const utilHttp = require("../util/utilHttp");
const constantes = require("../constantes.json");
const url = require('url'); 
const pathToFileURL = url.pathToFileURL;
const pathAPI = "/applications"
const fs = require("fs");

expressAppUtil.app.get(pathAPI, function (req, res) {
    try {
        var jsonString = fs.readFileSync(pathToFileURL(constantes.configuracion.pathApplication)).toString('utf8');
        utilHttp.resSuccess(res, req.user);
    } catch (error) {
        utilHttp.resTechnicalError(res, error);
    }
});

// middleware that is specific to this router
/*router.use(function timeLog(req, res, next) {
    console.log('Time: ', Date.now());
    next();  
});*/


// obtiene las aplicaciones
/*router.get('/applications', function (req, res) {
    try {
        var jsonString = fs.readFileSync(pathToFileURL(constantes.configuracion.pathApplication)).toString('utf8');
        utilHttp.resSuccess(res, JSON.parse(jsonString));
    } catch (error) {
        utilHttp.resTechnicalError(res, error);
    }
});*/

// actualiza las aplicaciones
/*router.post('/applications', function (req, res) {
    try {
        fs.writeFileSync(pathToFileURL(constantes.configuracion.pathApplication), req.body, 'utf8');
        utilHttp.resSuccess(res);
    } catch (error) {
        utilHttp.resTechnicalError(res, error);
    }
});

// obtiene los grupos
router.get('/groups', function (req, res) {
    try {
        var jsonString = fs.readFileSync(pathToFileURL(constantes.configuracion.pathGroups)).toString("utf8");
        utilHttp.resSuccess(res, JSON.parse(jsonString));
    } catch (error) {
        utilHttp.resTechnicalError(res, error);
    }
});

// actualiza los grupos
router.post('/groups', function (req, res) {
    try {
        fs.writeFileSync(pathToFileURL(constantes.configuracion.pathGroups), req.body, 'utf8');
        utilHttp.resSuccess(res);
    } catch (error) {
        utilHttp.resTechnicalError(res, error);
    }
});

// obtiene las opciones
router.get('/options', function (req, res) {
    try {
        var jsonString = fs.readFileSync(pathToFileURL(constantes.configuracion.pathOptions)).toString("utf8");
        utilHttp.resSuccess(res, JSON.parse(jsonString));
    } catch (error) {
        utilHttp.resTechnicalError(res, error);
    }
});

// actualiza las opnciones
router.post('/options', function (req, res) {
    try {
        fs.writeFileSync(pathToFileURL(constantes.configuracion.pathOptions), req.body, 'utf8');
        utilHttp.resSuccess(res);
    } catch (error) {
        utilHttp.resTechnicalError(res, error);
    }
});

// obtiene las roles
router.get('/roles', function (req, res) {
    try {
        var jsonString = fs.readFileSync(pathToFileURL(constantes.configuracion.pathRoles)).toString("utf8");
        utilHttp.resSuccess(res, JSON.parse(jsonString));
    } catch (error) {
        utilHttp.resTechnicalError(res, error);
    }
});

// actualiza las roles
router.post('/roles', function (req, res) {
    try {
        fs.writeFileSync(pathToFileURL(constantes.configuracion.pathRoles), req.body, 'utf8');
        utilHttp.resSuccess(res);
    } catch (error) {
        utilHttp.resTechnicalError(res, error);
    }
});

// obtiene los usuarios
router.get('/users', function (req, res) {
    try {
        var jsonString = fs.readFileSync(pathToFileURL(constantes.configuracion.pathUsers)).toString("utf8");
        utilHttp.resSuccess(res, JSON.parse(jsonString));
    } catch (error) {
        utilHttp.resTechnicalError(res, error);
    }
});

// actualiza los usuarios
router.post('/users', function (req, res) {
    try {
        fs.writeFileSync(pathToFileURL(constantes.configuracion.pathUsers), req.body, 'utf8');
        utilHttp.resSuccess(res);
    } catch (error) {
        utilHttp.resTechnicalError(res, error);
    }
});

*/