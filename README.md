sapwebide_local
===============

Comando para excutar aplicaciones locales como en SAP WEB IDE

[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)
[![Version](https://img.shields.io/npm/v/sapwebide_local.svg)](https://npmjs.org/package/sapwebide_local)
[![Downloads/week](https://img.shields.io/npm/dw/sapwebide_local.svg)](https://npmjs.org/package/sapwebide_local)
[![License](https://img.shields.io/npm/l/sapwebide_local.svg)](https://github.com/franzplt91/swil/blob/master/package.json)

<!-- toc -->
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->
# Usage
<!-- usage -->
```sh-session
$ npm install -g swil
$ swil COMMAND
running command...
$ swil (-v|--version|version)

$ swil --help [COMMAND]
USAGE
  $ swil or swil --path [PATH]
  - [PATH]
  - [PATH]/apps (applications)
  - [PATH]/destinations (destinations)
  - [PATH]/auth (authentication)

EXAMPLE
  $ swil
  $ swil --path ../my_folder
...
```
<!-- usagestop -->
# Commands
<!-- commands -->

<!-- commandsstop -->
