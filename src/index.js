
const expressUtil = require("../util/expressAppUtil");
const lineByLine = require('n-readlines');
var proxy = require('express-http-proxy');
const { createProxyMiddleware } = require('http-proxy-middleware');
const modifyResponse = require('node-http-proxy-json');
const chalk = require("chalk");
var DOMParser = require('xmldom').DOMParser;
const samlSP = require("../saml/saml");
const log = console.log;

const path = require('path');
const fs = require('fs');

const { Command, flags } = require('@oclif/command')
const iport = 3000;
class SapwebideLocalCommand extends Command {
  async run() {
    const { flags } = this.parse(SapwebideLocalCommand)
    var pathRoot = "../"+(flags.path || './');
    var  pathApps = pathRoot+"/apps"; // __dirname
    const directoryPath = path.join(__dirname, pathApps);
    console.log(pathRoot+'/auth/metadata_idp.xml');
    const directoryPathIDPMetadata = path.join(__dirname,pathRoot+'/auth/metadata_idp.xml');
    const xmlIDPMetadata = fs.readFileSync(directoryPathIDPMetadata, {encoding:'utf8'});
    var doc = new DOMParser().parseFromString( xmlIDPMetadata ,'text/xml');
    var URLAssertion = doc.getElementsByTagName("ns3:SingleSignOnService").item(0).getAttribute("Location");
    var CertIDP = doc.getElementsByTagName("ds:X509Certificate").item(0).textContent;
    expressUtil.applySAML(URLAssertion,CertIDP);
    expressUtil.redirectSAML("/","/apps");
    expressUtil.app.get('/saml/metadata', function (req, res) {
      var metadata = samlSP.samlMetadata(URLAssertion,CertIDP);
      res.setHeader("Content-Type", "application/xml");
      res.send(metadata);
  });

    //LLAMAMOS A LAS DIFERENTES APIS
    require("../apis/applicationsAPI");
    require("../apis/authAPI");

    log(chalk.blue('Folder') + ' ====> ' + chalk.green(directoryPath) + "\n");

    expressUtil.app.get('/',function(req,res,next){
      if(req.user){
        res.redirect("/apps");
      }
      next();
    });


    fs.readdir(directoryPath, function (err, files) {
      //handling error
      if (err) {
        return console.error('Unable to scan directory: ' + err);
      }
      //listing all files using forEach
      var urls = [];
      files.forEach(function (file) {
        log(chalk.blue('Aplicacion') + ' ====> ' + chalk.green(file));
        var nameapp = file;
        var neo_app = require(pathApps +"/"+ nameapp + "/neo-app.json");
        var contenedor = "/";
        var approute = "";
        expressUtil.app.get('/' + nameapp,function(req,res,next){
          if(!req.user){
            res.redirect("/");
          }
          next();
        });
        if (neo_app.welcomeFile) {
          log(chalk.blue('welcomeFile') + ' ====> ' + chalk.green(neo_app.welcomeFile));
          var arrayWF = neo_app.welcomeFile.split("/");
          contenedor = arrayWF.length === 1 ? "/" : neo_app.welcomeFile.replace(arrayWF[arrayWF.length - 1], "");
          approute = path.join(__dirname,pathApps + '/' + nameapp + contenedor);
          expressUtil.app.use('/' + nameapp, expressUtil.express.static(approute));
        }else{
          approute = path.join(__dirname,pathApps + '/' + nameapp);
          expressUtil.app.use('/' + nameapp, expressUtil.express.static(pathApps + '/' + nameapp));
        }

        log(chalk.blue('URL: ') + ' ====> ' + chalk.green('http://localhost:'+ iport + "/" + nameapp)+ "\n");
        urls.push('http://localhost:'+ iport + "/" + nameapp);
        if (neo_app.routes) {
          neo_app.routes.forEach(function (itemRoute) {
            //console.log(itemRoute);
            if (itemRoute.target.type === "destination") {

              var name = itemRoute.target.name;
              var pathProxy = itemRoute.path;
              var url = null;
              var tipoAuth = null;
              var user = null;
              var pass = null;
              const directoryPath2 = path.join(__dirname, pathRoot + "/destinations/") + name;
              
              const liner = new lineByLine(directoryPath2);
              var line = null;
              while (line = liner.next()) {
                line = line.toString();
                if (line[0] !== "#" && line.split) {
                  var atributo = line.split("=")[0]
                  var valor = line.replace(line.split("=")[0] + "=", "");
                  if (atributo === "URL") {
                    url = valor;
                  }

                  if (atributo === "Authentication") {
                    tipoAuth = valor;
                  }

                  if (atributo === "User") {
                    user = valor;
                  }

                  if (atributo === "Password") {
                    pass = valor;
                  }
                }
              }

              expressUtil.app.use(pathProxy, createProxyMiddleware({
                pathRewrite: { ['^' + pathProxy]: '/' },
                target: url,
                changeOrigin: true,
                secure: false,
                onProxyRes: function (proxyRes, req, res) {
                  modifyResponse(res, proxyRes, function (response) {
                    // modify response eventually
                    return response; // return value can be a promise
                  });
                },
                onProxyReq: function (proxyReq, req, res) {
                  if(!req.user){
                    res.redirect('http://localhost:'+ iport+"/apps");
                    return; 
                  }
                  if (tipoAuth === "BasicAuthentication") {
                    var stringAuth = Buffer.from(user + ":" + pass).toString('base64');
                    proxyReq.setHeader('Authorization', "Basic " + stringAuth);
                  }

                  if (tipoAuth === "AppToAppSSO") {
                    var stringAuth = Buffer.from(req.user.AssertionXml).toString('base64');
                    proxyReq.setHeader('authorization', "SAML2.0 " + stringAuth);
                    proxyReq.setHeader('x-proxybaseurl', "https://webidecp-z8gd3k7pes.dispatcher.br1.hana.ondemand.com/");
                  }
                  let bodyData;
                  if (!req.body || !Object.keys(req.body).length) {
                    return;
                  }
                  switch (proxyReq.getHeader('Content-Type')) {
                    case "application/json":
                    case "application/json; charset=UTF-8":
                      bodyData = JSON.stringify(req.body);
                      break;
                    case "application/x-www-form-urlencoded":
                      bodyData = querystring.stringify(req.body);
                      break;
                    default:
                  }

                  if (bodyData) {
                    //proxyReq.setHeader('Content-Type',req.header('Content-Type'));
                    proxyReq.setHeader('Content-Length', Buffer.byteLength(bodyData));
                    proxyReq.write(bodyData);
                    proxyReq.end();
                  }

                  // or log the req
                }
              }))
            }
          });
        }

        expressUtil.app.use('/' + nameapp + '/resources', proxy("https://sapui5.hana.ondemand.com/resources"));
      });

      expressUtil.app.get("/apps",function(req,res){
        if(!req.user){
          res.redirect("/");
          return;
        }
        var Html = "";
        urls.forEach(function(url){
          Html = Html + '<a href="'+url+'">'+url+'</a><br/><br/>';
        });
        res.send(Html);
      });

      expressUtil.app.get("/user", function (req, res) {
        res.send(req.user);
      });

      expressUtil.app.listen(iport, function () {
        console.log('Express server listening on port ' + 3000);
      });
    });
  }
}

SapwebideLocalCommand.description = `Describe the command here
...
Extra documentation goes here
`

SapwebideLocalCommand.flags = {
  // add --version flag to show CLI version
  version: flags.version({ char: 'v' }),
  // add --help flag to show CLI version
  help: flags.help({ char: 'h', description: 'ayuda de comandos' }),
  name: flags.string({ char: 'n', description: 'imprimir nombre' }),
  path: flags.string({ char: 'p', description: 'ruta de la subcuenta local' }),
}

module.exports = SapwebideLocalCommand
