exports.resSuccess = function (res, Data = null, sMessage = "OK") {
    res.statusCode = 200;
    res.statusMessage = sMessage;
    res.json(Data);
}

exports.resTechnicalError = function (res, Error = null, sMessage = "Ocurrio un error en el sistema") {
    res.statusCode = 500;
    res.statusMessage = sMessage;
    res.json({
        error: Error.toString(),
        trace: Error.stack
    });
}

exports.resFunctionalError = function (res, Data, sMessage, iCode) {
    res.statusCode = iCode;
    res.statusMessage = sMessage;
    res.json(Data);
}