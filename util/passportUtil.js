const samlSP = require("../saml/saml");
const passport = require('passport');

exports.applySAML = function (app, strategia) {
    app.use(passport.initialize());
    app.use(passport.session());

    passport.serializeUser(function (user, done) {
        done(null, user);
    });

    passport.deserializeUser(function (user, done) {
        done(null, user);
    });

    passport.use(strategia);
}