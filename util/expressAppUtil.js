const express = require('express');
const passport = require('passport');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
require('body-parser-xml')(bodyParser);
const session = require('express-session');
const constantes = require("../constantes.json");
var bRequireSaml = false;

var app = express();
app.use(cookieParser());
app.use(bodyParser.xml());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(session({
    resave: true,
    saveUninitialized: true,
    secret: constantes.configuracion.saml.secretSession
}));

exports.app = app;

exports.express = express;

exports.applySAML = function (UrlAssertion,Certificate) {
    const samlSP = require("../saml/saml");
    bRequireSaml = true;
    require("./passportUtil").applySAML(app, samlSP.samlStrategy(UrlAssertion,Certificate));
}

exports.get = app.get;

exports.post = app.post;

exports.put = app.put;

exports.delete = app.delete;

exports.redirectSAML = function (ruta,pathSuccess) {
    bodyParser.urlencoded({ extended: false }),
    app.get(ruta,
        passport.authenticate('saml', {
            successRedirect: pathSuccess,
            failureRedirect: constantes.configuracion.saml.failureRedirect
        })
    );
}

exports.getSAML = function (ruta, callback) {
    bodyParser.urlencoded({ extended: false }),
    app.get(ruta,
        passport.authenticate('saml', {
            failureRedirect: constantes.configuracion.saml.failureRedirect,
            failureFlash: true
        }),
        function (req, res) {
            callback(req,res);
        }
    );
}

exports.postSAML = function (ruta, callback) {
    bodyParser.urlencoded({ extended: false }),
    app.post(ruta,
        passport.authenticate('saml', {
            failureRedirect: constantes.configuracion.saml.failureRedirect,
            failureFlash: true
        }),
        function (req, res) {
            callback(req,res);
        }
    );
}

exports.putSAML = function (ruta, callback) {
    app.put(ruta,
        passport.authenticate('saml', {
            failureRedirect: constantes.configuracion.saml.failureRedirect,
            failureFlash: true
        }),
        function (req, res) {
            callback(req,res);
        }
    );
}

exports.deleteSAML = function (ruta, callback) {
    app.delete(ruta,
        passport.authenticate('saml', {
            failureRedirect: constantes.configuracion.saml.failureRedirect,
            failureFlash: true
        }),
        function (req, res) {
            callback(req,res);
        }
    );
}