const express = require('express');
const passport = require('passport');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
require('body-parser-xml')(bodyParser);
const session = require('express-session');
const constantes = require("../constantes.json");
var bRequireSaml = false;

var router = express.Router();
router.use(cookieParser());
router.use(bodyParser.xml());
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({
    extended: false
}));
router.use(session({
    resave: true,
    saveUninitialized: true,
    secret: constantes.configuracion.saml.secretSession
}));

exports.router = router;

exports.applySAML = function (UrlAssertion,Certificate) {
    const samlSP = require("../saml/saml");
    bRequireSaml = true;
    require("./passportUtil").applySAML(router, samlSP.samlStrategy(UrlAssertion,Certificate));
}

exports.get = router.get;

exports.post = router.post;

exports.put = router.put;

exports.delete = router.delete;

exports.redirectSAML = function (ruta,pathSuccess) {
    bodyParser.urlencoded({ extended: false }),
    router.get(ruta,
        passport.authenticate('saml', {
            successRedirect: pathSuccess,
            failureRedirect: constantes.configuracion.saml.failureRedirect
        })
    );
}

exports.getSAML = function (ruta, callback) {
    bodyParser.urlencoded({ extended: false }),
    router.get(ruta,
        passport.authenticate('saml', function(resp1,resp2){
            console.log("resp1", resp1);
            console.log("resp2", resp2);
        })
    );
}

exports.postSAML = function (ruta, callback) {
    bodyParser.urlencoded({ extended: false }),
    router.post(ruta,
        passport.authenticate('saml', {
            failureRedirect: constantes.configuracion.saml.failureRedirect,
            failureFlash: true
        }),
        function (req, res) {
            callback(req,res);
        }
    );
}

exports.putSAML = function (ruta, callback) {
    router.put(ruta,
        passport.authenticate('saml', {
            failureRedirect: constantes.configuracion.saml.failureRedirect,
            failureFlash: true
        }),
        function (req, res) {
            callback(req,res);
        }
    );
}

exports.deleteSAML = function (ruta, callback) {
    router.delete(ruta,
        passport.authenticate('saml', {
            failureRedirect: constantes.configuracion.saml.failureRedirect,
            failureFlash: true
        }),
        function (req, res) {
            callback(req,res);
        }
    );
}