const fs = require("fs");
var SamlStrategy = require('passport-saml').Strategy;
const constantes = require("../constantes.json");
var privateCert = fs.readFileSync(constantes.configuracion.saml.path_client_cert_key, 'utf8');
var client_cert = fs.readFileSync(constantes.configuracion.saml.path_client_cert, 'utf8');
function createStrategy(UrlAssertion, Certificado) {
    var sapIDPCert = Certificado;
    var sapEntryPoint = UrlAssertion;

    var strategia = new SamlStrategy({
        protocol: constantes.configuracion.saml.protocol,
        host: constantes.configuracion.saml.host,
        path: constantes.configuracion.saml.pathSaml,
        entryPoint: sapEntryPoint,
        issuer: constantes.configuracion.saml.protocol + constantes.configuracion.saml.host + '/sso/saml2/acs',
        signatureAlgorithm: constantes.configuracion.saml.signatureAlgorithm,
        // Service Provider Certificate
        privateCert: privateCert,
        // Identity Provider's public key
        cert: sapIDPCert,
        validateInResponseTo: false,
        disableRequestedAuthnContext: true
    },
        function (profile, done, error) {
            //console.log("profile: ", profile);
            if (error) {
                done(error);
            }

            profile.AssertionXml = profile.getAssertionXml();
            //profile.Assertion = profile.getAssertion();
            return done(null, profile);
        });

    return strategia;
}




exports.samlStrategy = createStrategy;


exports.samlMetadata = function (URLAssertion, Certificado) {
    return createStrategy(URLAssertion, Certificado).generateServiceProviderMetadata(null, client_cert)
};